import amqp = require("amqplib/callback_api");

amqp.connect(
  {
    protocol: "amqp",
    hostname: "localhost",
    port: 5672,
    username: "admin",
    password: "123456",
  },
  function (error0, connection) {
    if (error0) {
      throw error0;
    }
    connection.createChannel(function (error1, channel) {
      if (error1) {
        throw error1;
      }

      channel.assertExchange("header.exchange", "headers", {
        durable: true,
      });

      let output = "This is the message";
      let opts = { headers: { format: "pdf" } };
      channel.publish("header.exchange", "", Buffer.from(output), opts);
      
      const headersContent = JSON.stringify(opts)
      console.log(`Message sent: ${output} with headers: ${headersContent}`);
    });
    setTimeout(function () {
      connection.close();
      process.exit(0);
    }, 500);
  }
);
